﻿DROP DATABASE IF EXISTS gestionordenadores;
CREATE DATABASE gestionordenadores;

USE gestionordenadores;

CREATE OR REPLACE TABLE portatiles(
  idportatil int(10) AUTO_INCREMENT,
  estado int(10),
  aula int(10),
  numeroportatil int(10),
  procesador varchar(30),
  disco varchar(30),
  memoria varchar(30),
  PRIMARY KEY(idportatil)
);

CREATE OR REPLACE TABLE estados(
  idestado int(10) AUTO_INCREMENT,
  status boolean,
  descripcion varchar(70),
  observaciones varchar(100),
  PRIMARY KEY(idestado)
  );

CREATE OR REPLACE TABLE aulas(
  idaula int(10) AUTO_INCREMENT,
  numero int(10),
  descripcion varchar(40),
  PRIMARY KEY(idaula)
  );

CREATE OR REPLACE TABLE estudios(
  idestudio int(10) AUTO_INCREMENT,
  abreviatura varchar(30),
  turno int(10),
  descripcion varchar(100),
  finicio date,
  ffinal date,
  PRIMARY KEY(idestudio)
  );

CREATE OR REPLACE TABLE turnos(
  idturno int(10) AUTO_INCREMENT,
  nombre varchar(50),
  horario time,
  descripcion varchar(100),
  diassemana varchar(30),
  PRIMARY KEY(idturno)
  );

CREATE OR REPLACE TABLE prestamos(
  idprestamo int(10) AUTO_INCREMENT,
  portatil int(10),
  estudio int(10),
  alumno varchar(50) NOT NULL,
  PRIMARY KEY(idprestamo)
  );


  ALTER TABLE prestamos
  ADD CONSTRAINT FK_PRESTAMOS_PORTATILES
  FOREIGN KEY (portatil)
  REFERENCES portatiles(idportatil),

  ADD CONSTRAINT FK_PRESTAMOS_ESTUDIOS
  FOREIGN KEY(estudio)
  REFERENCES estudios(idestudio),
  
  ADD CONSTRAINT UK_PORTATILES_ESTUDIOS
  UNIQUE KEY(portatil, estudio);

  ALTER TABLE portatiles
  ADD CONSTRAINT FK_PORTATILES_AULAS
  FOREIGN KEY (aula)
  REFERENCES aulas(idaula);

  ALTER TABLE portatiles
  ADD CONSTRAINT FK_PORTATILES_ESTADOS
  FOREIGN KEY (estado)
  REFERENCES estados(idestado);

  ALTER TABLE estudios
  ADD CONSTRAINT FK_ESTUDIOS_TURNOS
  FOREIGN KEY (turno)
  REFERENCES turnos(idturno);

  SELECT prestamos.idprestamo, prestamos.portatil, prestamos.alumno,estudios.descripcion FROM prestamos INNER JOIN estudios ON prestamos.estudio = estudios.descripcion