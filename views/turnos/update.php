<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Turnos */

$this->title = 'Actualizar Turnos: ' . $model->idturno;
$this->params['breadcrumbs'][] = ['label' => 'Turnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idturno, 'url' => ['view', 'idturno' => $model->idturno]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="turnos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_turnos' => $model_turnos,
    ]) ?>

</div>
