<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Turnos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="turnos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'horario')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'diassemana')->textInput(['maxlength' => true]) ?>
     
    <?php
    
    $listaturnos=ArrayHelper::map($model_turnos,'idturno','nombre');?>
    
    <?= $form->field($model,'idturno')->dropDownList($listaturnos)->label('Turnos') ?>
    
     <?php>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
