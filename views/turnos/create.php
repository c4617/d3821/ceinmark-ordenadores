<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Turnos */

$this->title = 'Crear Turnos';
$this->params['breadcrumbs'][] = ['label' => 'Turnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="turnos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_turnos' => $model_turnos,
    ]) ?>

</div>
