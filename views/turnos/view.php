<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Turnos */

$this->title = $model->idturno;
$this->params['breadcrumbs'][] = ['label' => 'Turnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="turnos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idturno' => $model->idturno], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idturno' => $model->idturno], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querrer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idturno',
            'nombre',
            'horario',
            'descripcion',
            'diassemana',
        ],
    ]) ?>

</div>
