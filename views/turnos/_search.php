<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Turnos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="turnos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idturno') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'horario') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'diassemana') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetear', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
