<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Estudios */

$this->title = $model->idestudio;
$this->params['breadcrumbs'][] = ['label' => 'Estudios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estudios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idestudio' => $model->idestudio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idestudio' => $model->idestudio], [
            'class' => 'btn btn-danger',
            'data' => [
                'Confirmar' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idestudio',
            'abreviatura',
            'turno',
            'descripcion',
            'finicio',
            'ffinal',
        ],
    ]) ?>

</div>
