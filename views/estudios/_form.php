<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Estudios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estudios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'abreviatura')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    
    <?php
    
    $listaturnosc=ArrayHelper::map($model_turnosc,'idturno','nombre');?>
    
    <?= $form->field($model,'turno')->dropDownList($listaturnosc)->label('Turno') ?>
    
     <?php>
  
    <?= $form->field($model, 'finicio')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Fecha inicio del curso'],
    'pluginOptions' => [
    'autoclose' => true
     ]
    ]);?>
    
    <?= $form->field($model, 'ffinal')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Fecha final del curso'],
    'pluginOptions' => [
    'autoclose' => true
     ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

