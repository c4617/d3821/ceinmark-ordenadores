<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estudios */

$this->title = 'Actualizar Estudios: ' . $model->idestudio;
$this->params['breadcrumbs'][] = ['label' => 'Estudios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idestudio, 'url' => ['view', 'idestudio' => $model->idestudio]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="estudios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
         'model_turnosc' => $model_turnosc,
    ]) ?>

</div>
