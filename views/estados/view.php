<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Estados */

$this->title =$model->idestado;
$this->params['breadcrumbs'][] = ['label' => 'Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estados-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idestado' => $model->idestado], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idestado' => $model->idestado], [
            'class' => 'btn btn-primary',
            'data' => [
                'Confirmar' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idestado',
            'status',
            'descripcion',
            'observaciones',
        ],
    ]) ?>

</div>
