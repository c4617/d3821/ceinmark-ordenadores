<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Estados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    
    $listaestadosc=ArrayHelper::map($model_estadosc,'status','descripcion');?>
    
    <?= $form->field($model,'idestado')->dropDownList($listaestadosc)->label('Estado') ?>
    
     <?php>
    
    <?= $form->field($model, 'observaciones')->textInput(['maxlength' => true]) ?>
    

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
