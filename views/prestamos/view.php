<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\prestamos */

$this->title = $model->idprestamo;
$this->params['breadcrumbs'][] = ['label' => 'Prestamos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="prestamos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idprestamo' => $model->idprestamo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'idprestamo' => $model->idprestamo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro de querrer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idprestamo',
            'portatil',
            'estudio',
            'alumno',
        ],
    ]) ?>

</div>
