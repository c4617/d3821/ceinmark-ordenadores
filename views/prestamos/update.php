<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\prestamos */

$this->title = 'Actualizar Prestamo: ' . $model->idprestamo;
$this->params['breadcrumbs'][] = ['label' => 'Prestamos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idprestamo, 'url' => ['view', 'idprestamo' => $model->idprestamo]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="prestamos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_prestamos' => $model_prestamos,
        'model_estudios' => $model_estudios,
        'model_portatiles' => $model_portatiles,
    ]) ?>

</div>
