<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\prestamos */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="prestamos-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?php
    
    $listaalumnos=ArrayHelper::map($model_prestamos,'idprestamo','alumno');?>
    
    <?= $form->field($model,'idprestamo')->dropDownList($listaalumnos)->label('Alumno') ?>
    
     <?php
    
    $listacursos=ArrayHelper::map($model_estudios,'estudio','descripcion');?>
    
    <?= $form->field($model,'estudio')->dropDownList($listacursos)->label('Cursos') ?>

       
     <?php
    
    $listaportatiles=ArrayHelper::map($model_portatiles,'idportatil','descripcion');?>
    
    <?= $form->field($model,'portatil')->dropDownList($listaportatiles)->label('Ordenador') ?>
    
    
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
