<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Préstamos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestamos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Préstamos', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//          ['class' => 'yii\grid\SerialColumn'],

            'idprestamo',
            'alumno',
            [
                'attribute'=>'Curso',
                'value'=>'estudio0.descripcion',
            ],
            [
                'attribute'=>'Ordenador',
                'value'=>'portatil0.procesador',
            ],
            [
                'attribute'=>'FInicio',
                'value'=>'estudio0.finicio',
            ],
            [
                'attribute'=>'FFinal',
                'value'=>'estudio0.ffinal',
            ],
           // 'portatil',
           // 'estudio',
            [

                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                return Url::toRoute([$action, 'idprestamo' => $model->idprestamo]);
                }
            ],
        ],
    ]); ?>


</div>
