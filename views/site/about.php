<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

    $this->title = 'Prestamos de ordenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
  <form>
      
  <div class="form-row align-items-center">
    <div class="col-auto my-1">
      <label class="mr-sm-2" for="inlineFormCustomSelect">Nº Ordenador</label>
      <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
        <option selected>PC...</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </select>
    </div>
      
    <div class="form-row align-items-center">
    <div class="col-auto my-1">
      <label class="mr-sm-2" for="inlineFormCustomSelect">Curso</label>
      <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
        <option selected>Curso...</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </select>
    </div>
  </div>
  </div> 
      <div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text" id="">Nombre y apellidos</span>
  </div>
  <input type="text" class="form-control">
  <input type="text" class="form-control">
</div>
    
</div>
      
</form>
</div>
