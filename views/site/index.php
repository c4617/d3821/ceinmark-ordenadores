<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
$this->title = 'Gestión Prestamos';
?>
<div class="site-index">
 
    <div class="jumbotron text-center bg-transparent sombrastxt">
        <h1 class="display-4">Panel de Gestión Administrador</h1>
    </div>

    <div class="body-content">
        
        <div class="row">          
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima sombras">
                    <div class="card-body tarjeta">
                       <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/envio.gif"  height="100px">
                       <h3>PRÉSTAMOS</h3>  
                       <?= Html::a('Gestionar',['prestamos/index'],['class' =>'btn btn-primary'])?> 
                                             <!--site/consultaprestamos-->
                    </div>
               </div>
        </div>            
         <div class="col-sm-6 col-md-4">
                <div class="card alturaminima sombras">
                    <div class="card-body tarjeta">
                       <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/computadora.gif"  height="100px">
                       <h3>ORDENADORES</h3> 
                       <?= Html::a('Gestionar',['portatiles/index'],['class' =>'btn btn-primary'])?>
                    </div>
               </div>
        </div>           
         <div class="col-sm-6 col-md-4">
                <div class="card alturaminima sombras">
                    <div class="card-body tarjeta">
                        <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/colegio.gif"  height="100px">
                        <h3>AULAS</h3>
                        <?= Html::a('Gestionar',['aulas/index'],['class' =>'btn btn-primary'])?>
                    </div>
               </div>
        </div>            
         <div class="col-sm-6 col-md-4">
                <div class="card alturaminima sombras">
                    <div class="card-body tarjeta">
                       <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/24-horas.gif"  height="100px">
                       <h3>TURNOS</h3>
                       <?= Html::a('Gestionar',['turnos/index'],['class' =>'btn btn-primary'])?>
                    </div>
               </div>
        </div>            
         <div class="col-sm-6 col-md-4">
                <div class="card alturaminima sombras">
                    <div class="card-body tarjeta">
                       <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/advertencia.gif"  height="100px">
                       <h3>ESTADOS</h3>
                       <?= Html::a('Gestionar',['estados/index'],['class' =>'btn btn-primary'])?>
                    </div>
               </div>
        </div>    
          <div class="col-sm-6 col-md-4">
                <div class="card alturaminima sombras">
                    <div class="card-body tarjeta">
                      <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/carpetas.gif"  height="100px">
                       <h3>CURSOS</h3>
                       <?= Html::a('Gestionar',['estudios/index'],['class' =>'btn btn-primary'])?>
                    </div>
               </div>
        </div>
</div>
</div>
</div>