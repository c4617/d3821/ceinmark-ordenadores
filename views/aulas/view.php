<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Aulas */

$this->title = $model->idaula;
$this->params['breadcrumbs'][] = ['label' => 'Aulas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="aulas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idaula' => $model->idaula], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idaula' => $model->idaula], [
            'class' => 'btn btn-danger',
            'data' => [
                'Confirmar' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idaula',
            'numero',
            'descripcion',
        ],
    ]) ?>

</div>
