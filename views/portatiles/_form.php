<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Portatiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portatiles-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'numeroportatil')->textInput() ?>

    <?= $form->field($model, 'procesador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'memoria')->textInput(['maxlength' => true]) ?>
    
     <?php
    
    $listaestados=ArrayHelper::map($model_estados,'estado','descripcion');?>
    
    <?= $form->field($model,'estado')->dropDownList($listaestados)->label('Estados') ?>
    
     <?php>
     
    <?php
    
    $listaaulas=ArrayHelper::map($model_aulas,'numero','descripcion');?>
    
    <?= $form->field($model,'aula')->dropDownList($listaaulas)->label('Aulas') ?>
    
     <?php>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
