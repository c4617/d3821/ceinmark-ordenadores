<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ordenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portatiles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Ordenadores', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'idportatil',
          // 'estado',
          // 'aula',
            'numeroportatil',
            'procesador',
            'disco',
            'memoria',
            [
                'attribute'=>'Estado',
                'value'=>'estado0.descripcion',
            ],
            [
                'attribute'=>'Aula',
                'value'=>'aula0.numero',
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idportatil' => $model->idportatil]);
                 }
            ],
        ],
    ]); ?>


</div>
