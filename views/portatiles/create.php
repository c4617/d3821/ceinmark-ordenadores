<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Portatiles */

$this->title = 'Crear Ordenadores';
$this->params['breadcrumbs'][] = ['label' => 'Ordenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portatiles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_estados' => $model_estados,
        'model_aulas' => $model_aulas,
    ]) ?>

</div>
