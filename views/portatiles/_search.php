<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Portatiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portatiles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idportatil') ?>

    <?= $form->field($model, 'estado') ?>

    <?= $form->field($model, 'aula') ?>

    <?= $form->field($model, 'numeroportatil') ?>

    <?= $form->field($model, 'procesador') ?>

    <?php // echo $form->field($model, 'disco') ?>

    <?php // echo $form->field($model, 'memoria') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetear', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
