<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Portatiles */

$this->title = 'Actualizar Ordenador: ' . $model->idportatil;
$this->params['breadcrumbs'][] = ['label' => 'Ordenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idportatil, 'url' => ['view', 'idportatil' => $model->idportatil]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="portatiles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_estados' => $model_estados,
        'model_aulas' => $model_aulas,
    ]) ?>

</div>
