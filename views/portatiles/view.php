<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Portatiles */

$this->title = $model->idportatil;
$this->params['breadcrumbs'][] = ['label' => 'Portatiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="portatiles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idportatil' => $model->idportatil], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idportatil' => $model->idportatil], [
            'class' => 'btn btn-danger',
            'data' => [
                'Confirmar' => '¿Estas seguro de querer borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idportatil',
            'estado',
            'aula',
            'numeroportatil',
            'procesador',
            'disco',
            'memoria',
        ],
    ]) ?>

</div>
