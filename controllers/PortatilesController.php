<?php

namespace app\controllers;

use app\models\Portatiles;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Estados;
use app\models\Aulas;

/**
 * PortatilesController implements the CRUD actions for Portatiles model.
 */
class PortatilesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Portatiles models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Portatiles::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idportatil' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Portatiles model.
     * @param int $idportatil Idportatil
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idportatil)
    {
        return $this->render('view', [
            'model' => $this->findModel($idportatil),
        ]);
    }

    /**
     * Creates a new Portatiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Portatiles();
        $model_estados = Estados::find()->all();
        $model_aulas = Aulas::find()->all();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idportatil' => $model->idportatil]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'model_estados' => $model_estados,
            'model_aulas' => $model_aulas,
        ]);
    }

    /**
     * Updates an existing Portatiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idportatil Idportatil
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idportatil)
    {
        $model = $this->findModel($idportatil);
        $model_estados = Estados::find()->all();
        $model_aulas = Aulas::find()->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idportatil' => $model->idportatil]);
        }

        return $this->render('update', [
            'model' => $model,
            'model_estados' => $model_estados,
            'model_aulas' => $model_aulas,
        ]);
    }

    /**
     * Deletes an existing Portatiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idportatil Idportatil
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idportatil)
    {
        $this->findModel($idportatil)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Portatiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idportatil Idportatil
     * @return Portatiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idportatil)
    {
        if (($model = Portatiles::findOne(['idportatil' => $idportatil])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
