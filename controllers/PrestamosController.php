<?php

namespace app\controllers;

use app\models\prestamos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Estudios;
use app\models\Portatiles;
/**
 * PrestamosController implements the CRUD actions for prestamos model.
 */
class PrestamosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all prestamos models.
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => prestamos::find(),
     
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single prestamos model.
     * @param int $idprestamo Idprestamo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idprestamo)
    {
        return $this->render('view', [
            'model' => $this->findModel($idprestamo),
        ]);
    }

    /**
     * Creates a new prestamos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new prestamos();
        $model_prestamos = Prestamos::find()->all();
        
        $model_estudios = Estudios::find()->all();
        $model_portatiles = Portatiles::find()->all();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idportatil' => $model->idportatil]);
            }
        } else {
            $model->loadDefaultValues();
        }
        
        return $this->render('create', [
            'model' => $model,
            'model_prestamos' => $model_prestamos,
            'model_estudios' => $model_estudios,
            'model_portatiles' => $model_portatiles,
            
        ]);
    }

    /**
     * Updates an existing prestamos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idprestamo Idprestamo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idprestamo)
    {
        $model = $this->findModel($idprestamo);
         $model_prestamos = Prestamos::find()->all();
        
        $model_estudios = Estudios::find()->all();
        $model_portatiles = Portatiles::find()->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idprestamo' => $model->idprestamo]);
        }

        return $this->render('update', [
            'model' => $model,
            'model_prestamos' => $model_prestamos,
            'model_estudios' => $model_estudios,
            'model_portatiles' => $model_portatiles,
        ]);
    }

    /**
     * Deletes an existing prestamos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idprestamo Idprestamo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idprestamo)
    {
        $this->findModel($idprestamo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the prestamos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idprestamo Idprestamo
     * @return prestamos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idprestamo)
    {
        if (($model = prestamos::findOne(['idprestamo' => $idprestamo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Esta página no existe');
    }
}
