<?php

namespace app\controllers;

use app\models\Turnos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TurnosController implements the CRUD actions for Turnos model.
 */
class TurnosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Turnos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Turnos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idturno' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Turnos model.
     * @param int $idturno Idturno
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idturno)
    {
        return $this->render('view', [
            'model' => $this->findModel($idturno),
        ]);
    }

    /**
     * Creates a new Turnos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Turnos();
        $model_turnos = Turnos::find()->all();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idturno' => $model->idturno]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'model_turnos' => $model_turnos,
        ]);
    }

    /**
     * Updates an existing Turnos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idturno Idturno
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idturno)
    {
        $model = $this->findModel($idturno);
        $model_turnos = Turnos::find()->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idturno' => $model->idturno]);
        }

        return $this->render('update', [
            'model' => $model,
             'model_turnos' => $model_turnos,
        ]);
    }

    /**
     * Deletes an existing Turnos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idturno Idturno
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idturno)
    {
        $this->findModel($idturno)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Turnos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idturno Idturno
     * @return Turnos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idturno)
    {
        if (($model = Turnos::findOne(['idturno' => $idturno])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Esta página no existe.');
    }
}
