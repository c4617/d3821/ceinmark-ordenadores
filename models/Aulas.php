<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aulas".
 *
 * @property int $idaula
 * @property int|null $numero
 * @property string|null $descripcion
 *
 * @property Portatiles[] $portatiles
 */
class Aulas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aulas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero'], 'integer'],
            [['descripcion'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idaula' => 'Id aula',
            'numero' => 'Número',
            'descripcion' => 'Descripción',
        ];
    }

    /**
     * Gets query for [[Portatiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatiles()
    {
        return $this->hasMany(Portatiles::className(), ['aula' => 'idaula']);
    }
}
