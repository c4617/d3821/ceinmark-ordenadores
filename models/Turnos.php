<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "turnos".
 *
 * @property int $idturno
 * @property string|null $nombre
 * @property string|null $horario
 * @property string|null $descripcion
 * @property string|null $diassemana
 *
 * @property Estudios[] $estudios
 */
class Turnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'turnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['horario'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
            [['descripcion'], 'string', 'max' => 100],
            [['diassemana'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idturno' => 'Turno',
            'nombre' => 'Nombre',
            'horario' => 'Horario',
            'descripcion' => 'Descripción',
            'diassemana' => 'Dias Semana',
        ];
    }

    /**
     * Gets query for [[Estudios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudios()
    {
        return $this->hasMany(Estudios::className(), ['turno' => 'idturno']);
    }
}
