<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "portatiles".
 *
 * @property int $idportatil
 * @property int|null $estado
 * @property int|null $aula
 * @property int|null $numeroportatil
 * @property string|null $procesador
 * @property string|null $disco
 * @property string|null $memoria
 *
 * @property Aulas $aula0
 * @property Estados $estado0
 * @property Estudios[] $estudios
 * @property Prestamos[] $prestamos
 */
class Portatiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portatiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado', 'aula', 'numeroportatil'], 'integer'],
            [['procesador', 'disco', 'memoria'], 'string', 'max' => 30],
            [['aula'], 'exist', 'skipOnError' => true, 'targetClass' => Aulas::className(), 'targetAttribute' => ['aula' => 'idaula']],
            [['estado'], 'exist', 'skipOnError' => true, 'targetClass' => Estados::className(), 'targetAttribute' => ['estado' => 'idestado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idportatil' => 'Ordenador',
            'estado' => 'Estado',
            'aula' => 'Aula',
            'numeroportatil' => 'Serie',
            'procesador' => 'Procesador',
            'disco' => 'Disco',
            'memoria' => 'Memoria',
        ];
    }

    /**
     * Gets query for [[Aula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAula0()
    {
        return $this->hasOne(Aulas::className(), ['idaula' => 'aula']);
    }

    /**
     * Gets query for [[Estado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstado0()
    {
        return $this->hasOne(Estados::className(), ['idestado' => 'estado']);
    }

    /**
     * Gets query for [[Estudios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudios()
    {
        return $this->hasMany(Estudios::className(), ['idestudio' => 'estudio'])->viaTable('prestamos', ['portatil' => 'idportatil']);
    }

    /**
     * Gets query for [[Prestamos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrestamos()
    {
        return $this->hasMany(Prestamos::className(), ['portatil' => 'idportatil']);
    }
}
