<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudios".
 *
 * @property int $idestudio
 * @property string|null $abreviatura
 * @property int|null $turno
 * @property string|null $descripcion
 * @property string|null $finicio
 * @property string|null $ffinal
 *
 * @property Portatiles[] $portatils
 * @property Prestamos[] $prestamos
 * @property Turnos $turno0
 */
class Estudios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turno'], 'integer'],
            [['finicio', 'ffinal'], 'safe'],
            [['abreviatura'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 100],
            [['turno'], 'exist', 'skipOnError' => true, 'targetClass' => Turnos::className(), 'targetAttribute' => ['turno' => 'idturno']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestudio' => 'Estudio',
            'abreviatura' => 'Abreviatura',
            'turno' => 'Turno',
            'descripcion' => 'Nombre del curso',
            'finicio' => 'Fecha Inicio',
            'ffinal' => 'Fecha Final',
        ];
    }

    /**
     * Gets query for [[Portatils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatils()
    {
        return $this->hasMany(Portatiles::className(), ['idportatil' => 'portatil'])->viaTable('prestamos', ['estudio' => 'idestudio']);
    }

    /**
     * Gets query for [[Prestamos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrestamos()
    {
        return $this->hasMany(Prestamos::className(), ['estudio' => 'idestudio']);
    }

    /**
     * Gets query for [[Turno0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTurno0()
    {
        return $this->hasOne(Turnos::className(), ['idturno' => 'turno']);
    }
}
