<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estados".
 *
 * @property int $idestado
 * @property int|null $status
 * @property string|null $descripcion
 * @property string|null $observaciones
 *
 * @property Portatiles[] $portatiles
 */
class Estados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['descripcion'], 'string', 'max' => 70],
            [['observaciones'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestado' => 'Estado',
            'status' => 'Status',
            'descripcion' => 'Descripcion',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * Gets query for [[Portatiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatiles()
    {
        return $this->hasMany(Portatiles::className(), ['estado' => 'idestado']);
    }
}
