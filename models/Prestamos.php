<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prestamos".
 *
 * @property int $idprestamo
 * @property int|null $portatil
 * @property int|null $estudio
 * @property string $alumno
 *
 * @property Estudios $estudio0
 * @property Portatiles $portatil0
 */
class Prestamos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prestamos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['portatil', 'estudio'], 'integer'],
            [['alumno'], 'required'],
            [['alumno'], 'string', 'max' => 50],
            [['portatil', 'estudio'], 'unique', 'targetAttribute' => ['portatil', 'estudio']],
            [['estudio'], 'exist', 'skipOnError' => true, 'targetClass' => Estudios::className(), 'targetAttribute' => ['estudio' => 'idestudio']],
            [['portatil'], 'exist', 'skipOnError' => true, 'targetClass' => Portatiles::className(), 'targetAttribute' => ['portatil' => 'idportatil']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprestamo' => 'Préstamo',
            'portatil' => 'Ordenador',
            'estudio' => 'Curso',
            'alumno' => 'Alumno',
        ];
    }

    /**
     * Gets query for [[Estudio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudio0()
    {
        return $this->hasOne(Estudios::className(), ['idestudio' => 'estudio']);
        //  cruds mirar se tiene que hacer en la vista con las funciones del modelo
    }

    /**
     * Gets query for [[Portatil0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatil0()
    {
        return $this->hasOne(Portatiles::className(), ['idportatil' => 'portatil']);
    }
    
    
}
